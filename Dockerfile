FROM node:14-alpine3.11

RUN apk update && apk add --no-cache python make g++ 

ADD ./source/ /app/

WORKDIR /app

RUN npm install

EXPOSE 4200

CMD ["npm", "run", "start", "--", "--host", "0.0.0.0", "--disableHostCheck"]




